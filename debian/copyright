Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: nacl
Source: https://nacl.cr.yp.to/

Files: *
Copyright: public-domain
License: public-domain

Files: crypto_scalarmult/curve25519/donna_c64/smult.c
Copyright: 2008, Google Inc.
License: public-domain

Files: debian/*
Copyright: 2016-2024, Jan Mojžíš <jan.mojzis@gmail.com>
  2012-2015, Sergiusz Pawlowicz <debian@pawlowicz.name>
License: GPL-2

Files: debian/man/nacl-sha256.1
  debian/man/nacl-sha512.1
Copyright: Sergiusz Pawlowicz
License: GPL-2

License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: public-domain
 Public domain.
 .
 All of the NaCl software is in the public domain, see more at:
 https://nacl.cr.yp.to/features.html
