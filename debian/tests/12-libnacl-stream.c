#include <crypto_stream.h>
#include <crypto_stream_salsa2012.h>
#include <crypto_stream_salsa208.h>
#include <crypto_stream_salsa20.h>
#include <crypto_stream_xsalsa20.h>
#include <randombytes.h>
#include <stdio.h>

#define SPACE 10001
#define ALIGNMENT 16

static unsigned char k[ALIGNMENT + 32] __attribute__((aligned(ALIGNMENT)));
static unsigned char n[ALIGNMENT + 24] __attribute__((aligned(ALIGNMENT)));
static unsigned char c[ALIGNMENT + SPACE] __attribute__((aligned(ALIGNMENT)));

int main(void) {

    long long i;

    for (i = 0; i < ALIGNMENT; ++i) {
        printf("crypto_stream_salsa20   test, offset = %lld ... begin\n", i); fflush(stdout);
        randombytes(k, sizeof k);
        randombytes(n, sizeof n);
        crypto_stream_salsa20(c + i, SPACE, n + i, k + i);
        randombytes(k, sizeof k);
        randombytes(n, sizeof n);
        crypto_stream_salsa20_xor(c + i, c + i, SPACE, n + i, k + i);
        printf("crypto_stream_salsa20   test, offset = %lld ... OK\n", i); fflush(stdout);

        printf("crypto_stream_salsa2012 test, offset = %lld ... begin\n", i); fflush(stdout);
        randombytes(k, sizeof k);
        randombytes(n, sizeof n);
        crypto_stream_salsa2012(c + i, SPACE, n + i, k + i);
        randombytes(k, sizeof k);
        randombytes(n, sizeof n);
        crypto_stream_salsa2012_xor(c + i, c + i, SPACE, n + i, k + i);
        printf("crypto_stream_salsa2012 test, offset = %lld ... OK\n", i); fflush(stdout);

        printf("crypto_stream_salsa208  test, offset = %lld ... begin\n", i); fflush(stdout);
        randombytes(k, sizeof k);
        randombytes(n, sizeof n);
        crypto_stream_salsa208(c + i, SPACE, n + i, k + i);
        randombytes(k, sizeof k);
        randombytes(n, sizeof n);
        crypto_stream_salsa208_xor(c + i, c + i, SPACE, n + i, k + i);
        printf("crypto_stream_salsa208  test, offset = %lld ... OK\n", i); fflush(stdout);
    }

    return 0;
}
