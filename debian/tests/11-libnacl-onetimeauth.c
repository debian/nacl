#include <crypto_onetimeauth.h>
#include <crypto_onetimeauth_poly1305.h>
#include <randombytes.h>
#include <stdio.h>

#define SPACE 10001
#define ALIGNMENT 16

static unsigned char k[ALIGNMENT + crypto_onetimeauth_poly1305_KEYBYTES] __attribute__((aligned(ALIGNMENT)));
static unsigned char a[ALIGNMENT + crypto_onetimeauth_poly1305_BYTES] __attribute__((aligned(ALIGNMENT)));
static unsigned char m[ALIGNMENT + SPACE] __attribute__((aligned(ALIGNMENT)));

int main(void) {

    long long i;

    for (i = 0; i < ALIGNMENT; ++i) {
        randombytes(k, sizeof k);
        randombytes(m, sizeof m);

        printf("onetimeauth test, offset = %lld ... begin\n", i); fflush(stdout);
        crypto_onetimeauth_poly1305(a + i, m + i, SPACE, k + i);

        if (crypto_onetimeauth_poly1305_verify(a + i, m + i, SPACE, k + i) != 0) {
            fprintf(stderr, "crypto_onetimeauth_poly1305_verify: ciphertext fails verification\n");
            return 1;
        }
        printf("onetimeauth test, offset = %lld ... OK\n", i); fflush(stdout);
    }

    return 0;
}
