nacl (20110221-15) unstable; urgency=medium

  * d/p/0002-fix-ftbfs.patch: update, fix cpuid/unknown.c compilation
    due to -Wimplicit-int
  * d/control: bump Standards-Version: 4.7.0, no changes
  * d/gbp.conf: dist = DEP14, debian-branch = debian/latest

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Thu, 24 Oct 2024 00:15:00 +0200

nacl (20110221-14) unstable; urgency=medium

  * d/p/0002-fix-ftbfs.patch add, fix FTBFS due to
    -Werror=implicit-function-declaration (Closes: 1066444)
  * d/gbp.conf: add [pull] track-missing = True
  * d/copyright: bump my copyright years

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Sat, 30 Mar 2024 09:17:37 +0100

nacl (20110221-13) unstable; urgency=medium

  * disable cpucycles build and use library libcpucycles
    * d/control add Build-Depends: libcpucycles-dev
    * d/p/0005-fix-cpucycles-mips.patch remove
    * d/p/0006-add-killafter.patch remove
    * d/p/0008-add-cpucycles-fakenanoseconds.patch remove
    * d/p/0013-don-t-use-cpucycles.o-to-compile-binaries.patch remove
    * d/p/0100-cpucycles-disable-build.patch add
  * disable randombytes build and use library librandombytes
    * d/control add Build-Depends: librandombytes-dev
    * d/p/debian/patches/0002-add-librandombytes.a.patch remove
    * d/p/0101-randombytes-disable-build.patch add
  * d/rules add DEB_LDFLAGS_MAINT_APPEND = -Wl,-z,noexecstack
  * d/rules move installation libraries and header files
    to debian/libnacl-dev.install
  * d/control bump Standards-Version to 4.6.2, no changes
  * d/copyright bump my copyright year

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Wed, 18 Oct 2023 06:12:04 +0200

nacl (20110221-12) unstable; urgency=medium

  * d/fix.scanned.copyright added
  * d/debian/copyright-scan-patterns.yml added
  * d/copyright updated, generated using 'cme update dpkg-copyright'

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Sun, 11 Dec 2022 21:43:54 +0100

nacl (20110221-11) unstable; urgency=medium

  * d/p/0013-don-t-use-cpucycles.o-to-compile-binaries.patch added, fixes
    reproducible build
  * d/control: bump Standards-Version to 4.6.1, no changes
  * d/rules: reenable i386 ASM implementation disabled in previous deb. versions
  * d/rules: ar/ranlib don't work with LTO objects,
    added DEB_BUILD_MAINT_OPTIONS = optimize=-lto (Closes: 1015554)

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Thu, 21 Jul 2022 08:45:15 +0200

nacl (20110221-10) unstable; urgency=medium

  * d/p/0011-limit-compilerfilename-to-120-bytes.patch updated and renamed
    to d/p/0011-limit-compilerfilename-to-100-bytes.patch. Now limits the
    filename to 100 bytes.
  * d/p/0010-strip-libnacl.a-before-compiling-binaries.patch updated,
    stipped all libraries in the directory. Not only libnacl.a.
  * The ASM implementation crypto_scalarmult/curve25519/athlon
    has been disabled, has problems with PIE.
  * d/tests/control added 'Restrictions: allow-stderr' for library tests.

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Fri, 20 May 2022 17:29:40 +0200

nacl (20110221-9) unstable; urgency=medium

  * In the previous version failed regression test on armhf and i386.
    This version fixes problems on both architectures. (Closes: 1010970).
    The ASM implementation crypto_onetimeauth/poly1305/x86 has been disabled,
    because has problems with PIE. hardening=+all reenabled.
    And removed crypto_stream_aes128ctr.h include from autopkg tests,
    because on armhf doesn't exist.
  * d/rules added more debug informations from dh_auto_build.

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Mon, 16 May 2022 18:08:42 +0200

nacl (20110221-8) unstable; urgency=medium

  * d/p/0011-limit-compilerfilename-to-120-bytes.patch added, Nacl has it's
    own build battery and in the build process creates files derived from
    the compiler name and parameters. When the filename is greater than 256,
    compilation fails on architectures m68k, sh4, x32 (File name too long).
    The patch limits the filename to 120 bytes.
  * d/p/0012-reject-primitives-with-alignment-problems.patch added. Rejects
    primitives with memory alignment problems.
  * d/control - libnacl-dev: remove Multi-Arch: same
  * d/rules - disabled hardening=+all. Nacl contains ASM code which stop
    working when hardening is enabled.
  * d/tests/00-nacl removed.
  * d/t/10-libnacl-print-implementation added. Tests whether the library
    prints implementation names for all primitives.
  * d/t/11-libnacl-onetimeauth added. Tests whether the crypto_onetimeauth
    works.
  * d/t/12-libnacl-stream added. Tests whether the crypto_stream works.
  * d/t/13-libnacl-box added. Tests whether the crypto_box works.

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Thu, 05 May 2022 17:53:02 +0200

nacl (20110221-7) unstable; urgency=medium


  [ Jan Mojžíš ]
  * d/patches rebuild, removed:
    d/patches/0002-remove-superfluous-compiler-flags.patch,
    d/patches/0003-output-to-standard-output-while-building.patch,
    d/patches/0006-fakegettimeofday.patch,
    d/patches/0011-curvecp-enable-debug-symbols.patch,
    d/patches/0012-commandline-enable-debug-symbols.patch,
    d/patches/0014-removed-unnecessary-implementations-to-allow-reprodu.patch,
    and functionality moved to d/rules
  * d/patches - added 0009-return-correct-client-extension.patch
  * d/patches - added 0010-strip-libnacl.a-before-compiling-binaries.patch
  * d/rules - install static libs. to the directory /usr/lib/<arch>/..
  * d/nacl-tools.lintian-overrides - override hardening-no-fortify-functions
  * d/source/lintian-overrides - override maintainer-manual-page
  * d/control - added Rules-Requires-Root: no
  * d/control - bump to standards version 4.6.0
  * d/control - bump debhelper compat level to 13 and use debhelper-compat
  * d/control - removed Dependency: ${shlibs:Depends} for libnacl-dev
  * d/compat - removed
  * d/control - libnacl-dev: added Multi-Arch: same
  * d/tests - removed curvecptest
  * d/tests - added 00-nacl
  * d/tests - added 01-curvecp-downloadtest
  * d/tests - added 02-curvecp-uploadtest
  * d/tests - added 03-curvecp-clientext
  * d/nacl-tools.install - install all binaries to /usr/bin
  * d/copyright - bump my copyright year

  [ Debian Janitor ]
  * Fix day-of-week for changelog entry 20110221-1.

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Thu, 30 Dec 2021 08:51:33 +0100

nacl (20110221-6.1) unstable; urgency=medium

  * Non-maintainer upload
  * Update patch to remove superfluous build flags, thanks to Laurent Bigonville
    (Closes: #920000)

 -- Hilko Bengen <bengen@debian.org>  Sun, 24 Feb 2019 10:54:10 +0100

nacl (20110221-6) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/tests: Use AUTOPKGTEST_TMP instead of ADTTMP.

  [ Jan Mojžíš ]
  * d/watch: added
  * Convert git repository from git-dpm to gbp layout
  * d/copyright: Bump my copyright year
  * Autopkgtest - rework
  * d/copyright - fixed url http->https
  * d/control - Vcs-* moved to salsa.debian.org
  * d/compat, d/control - debhelper >= 11
  * d/control - priority: optional
  * d/control - bump to standards version 4.2.1
  * Cpucycles library workaround for successful compilation
    on more architectures (Closes: #699397)
  * Disabled superfluous implementations to allow
    reproducible build (Closes: #774581)
  * d/lintian-overrides removed
  * d/source/lintian-overrides removed

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Tue, 23 Oct 2018 15:04:30 +0200

nacl (20110221-5) unstable; urgency=low

  * Updated upstream url (Closes: #824088)
  * Added librandombytes.a (Closes: #694248)
  * Fixed socket_bind library for curvecp* (Closes: #819045)
  * Cpucycles fix for mips architecture.
  * Cpucycles library workaround for successful compilation
    on more architectures (Closes: #678123)
  * Updated to standard version 3.9.8
  * Updated to debhelper 9
  * New maintainer (Closes: #832928)
  * Workaround for handling zombies in curvecpmessage (Closes: #819051)
  * Source switched to git-dpm style
  * Added Vcs-* fields
  * Cleanup in debian/rules
  * Changes in debian/copyright, bump to copyright-format 1.0
  * Man pages moved to debian/man/*
  * Removed build-dependency on docbook-to-man
  * nacl-tools moved to section net
  * Added autopkgtest

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Fri, 29 Jul 2016 09:10:00 +0200

nacl (20110221-4.1) unstable; urgency=low

  * Non-maintainer upload
  * Modify the build system to echo stuff to standard output during the build
    rather than only at the end. Hopefully this should prevent the timeout
    on mips.
  * Add some tests for the state of /sys before starting the build to help
    debug failures related to being unable to determine CPU frequency

 -- Peter Michael Green <plugwash@debian.org>  Fri, 21 Sep 2012 08:43:21 +0000

nacl (20110221-4) unstable; urgency=low

  * Move to unstable.

 -- Bartosz Fenski <fenio@debian.org>  Thu,  5 Apr 2012 09:23:19 +0200

nacl (20110221-3) experimental; urgency=low

  * Remove superflous compiler flags not available on all archs.

 -- Bartosz Fenski <fenio@debian.org>  Fri, 27 Jan 2012 10:04:19 +0100

nacl (20110221-2) experimental; urgency=low

  * Added verbosity to the build process, it will help debugging on builders.

 -- Sergiusz Pawlowicz <debian@pawlowicz.name>  Sat, 14 Jan 2012 03:05:09 +0000

nacl (20110221-1) experimental; urgency=low

  * 20110221 NaCl package prepared from scratch (Closes: #655187)

 -- Sergiusz Pawlowicz <debian@pawlowicz.name>  Wed, 11 Jan 2012 01:11:12 +0000
